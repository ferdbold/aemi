<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
if ('aemi' == $_SERVER['HTTP_HOST']) {
	define('DB_NAME', 'd23h8v2n_aemiuqac');
	define('DB_USER', 'root');
	define('DB_PASSWORD', 'root');
	define('DB_HOST', 'localhost');
	define('WP_SITEURL', 'http://aemi');
	define('WP_HOME', 'http://aemi');
} else {
	define('DB_NAME', 'd23h8v2n_aemiuqac');
	define('DB_USER', '');
	define('DB_PASSWORD', '');
	define('DB_HOST', 'localhost');
	define('WP_SITEURL', '');
	define('WP_HOME', '');
}

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/** WordPress subdirectory install */
define('WP_CONTENT_URL', WP_HOME.'/wp-content');
define('WP_CONTENT_DIR', realpath(ABSPATH.'../wp-content'));
define('ADMIN_COOKIE_PATH', '/');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'MbSp,|Ldc`hv*Oh_<>emi?Rf_4-cZ@;gbKhU-Xv6mc8pg#4Il4>$@V5r}yz+1!6O');
define('SECURE_AUTH_KEY',  'Hy-Fd)ta]R^J^u?&1Y`P*IPsm0&&|.`68?UE7:+VHL>gx0  s`$5#:YS3oC{Li7R');
define('LOGGED_IN_KEY',    's<gVi}A6JA7[}4:e6zi|>.}0^r%?1oRe+@N#?mu`,Ma}Ks|%|Bu3#i%Q65G|oEy*');
define('NONCE_KEY',        '0j1R)+p0M|jeuE3$r%Mg5IO-(E=;S3}e+qMlmPEAq^_}f@{Iry>?tE[k35.07xl$');
define('AUTH_SALT',        ':)ceNcPU+Pe`-tA6U$Fq6WRV,roA_.U-B _aOe8%43Zf5k|3$mBW96aQklQH fdV');
define('SECURE_AUTH_SALT', '!+]2]To72Frp&|aj)^-&m8/ytLEzEoukw~Ji6HjRLcGUrC{|Gm{Wi;Zk,|~{:n,d');
define('LOGGED_IN_SALT',   '~pezlq+T5i3]~QSjViU^`cX<6XWI- _1netY22;|)^;+q7?oW|k:VO}&0~G_70=b');
define('NONCE_SALT',       'G.tBq.E!41 /Bv|<~%}Aj)K{{3/eK-ZNV~{H:HPp;%u;kh-($B!W6Koi+M45*(GG');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', 'fr_FR');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

define('WP_MEMORY_LIMIT', '64M');

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
